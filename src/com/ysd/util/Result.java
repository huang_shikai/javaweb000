package com.ysd.util;

public class Result {
	private int code;
	private String msg;
	private int count;
	private Object data;
	public Result(int code, String msg, int conut, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.count = conut;
		this.data = data;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getConut() {
		return count;
	}
	public void setConut(int conut) {
		this.count = conut;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	 
}
